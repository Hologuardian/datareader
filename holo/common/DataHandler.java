package holo.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import org.newdawn.slick.Color;

public class DataHandler 
{
	private static final DataHandler instance = new DataHandler();

	public ArrayList<int[]> data = new ArrayList<int[]>();
	public ArrayList<Double> clicksPerSecond = new ArrayList<Double>();
	public ArrayList<Double> timeValues = new ArrayList<Double>();
	public ArrayList<Integer> intensityValues = new ArrayList<Integer>();
	public ArrayList<Color> colours = new ArrayList<Color>();
	public ArrayList<Integer> hoursPlayedValues = new ArrayList<Integer>();
	public ArrayList<Boolean> likesIntenseValues = new ArrayList<Boolean>();

	public double corrCoefIntenseClicks = 0;
	public double corrCoefHoursClicks = 0;
	public double corrCoefIntenseTime = 0;
	public double corrCoefHoursTime = 0;
	
	public double clicksPerSecondMean = 0;
	public double varianceClicks = 0;
	public double stDevClicks = 0;
	
	public double intensityMean = 0;
	public double varianceIntensity = 0;
	public double stDevIntensity = 0;
	
	public double hoursMean = 0;
	public double varianceHours = 0;
	public double stDevHours = 0;
	
	public double timeMean = 0;
	public double varianceTime = 0;
	public double stDevTime = 0;
	
	public int totalDatums = 0;
	
	public static DataHandler instance()
	{
		return instance;
	}

	public boolean readInfo()
	{
		BufferedReader sc = null;
		String url = "http://mark2.the-seamans.com/log/filename.txt";
		try
		{
			InputStreamReader is;
			try
			{
				URL loc= new URL(url);
				is = new InputStreamReader(loc.openStream());
			}
			catch(Exception e)
			{
				File file = new File("Raw.txt");
				if (!file.exists())
					return false;
				is = new InputStreamReader(new FileInputStream(file));
			}
			
			sc = new BufferedReader(is);
			
			String line = sc.readLine();
			int i = 0;
			
			while (line != null)
			{
				String[] sections = line.split(":");
				int[] datum = new int[7];
				datum[0] = i;
				for (int j = 1; j < 7; j++)
				{
					datum[j] = Integer.parseInt(sections[j]);
				}
				if (datum[5] > 84)
					datum[5] = 84;
				if (datum[1] != 0 || datum[2] != 0)
				{
					data.add(datum);
					totalDatums++;
				}
				line = sc.readLine();
			}
			
			is.close();
			sc.close();
			return true;
		}
		catch (IOException eio)
		{
			eio.printStackTrace();
		}
		return false;
	}
	
	public void load()
	{
		for(int[] datum : data)
		{
			@SuppressWarnings("unused")
			int user = datum[0];
			int intensity = datum[1];
			int timeMillis = datum[2];
			if (timeMillis < 1000)
				timeMillis += 1000;
			double time = timeMillis / 1000;
			int clicks = datum[3];
			int colourHash = datum[4];
			int hoursPlayed = datum[5];
//			boolean likesIntense = datum[6] == 1 ? true : false;
			
			clicksPerSecond.add(clicks / time);
			intensityValues.add(intensity);
			hoursPlayedValues.add(hoursPlayed);
			timeValues.add(time);
//			likesIntenseValues.add(likesIntense);
			colours.add(new Color(colourHash));
		}
	}
	
	public void do_discreetData()
	{
		double sumClicks = 0;
		double sumSquaredClicks = 0;
		
		int sumIntensity = 0;
		int sumSquaredIntensity = 0;
		
		int sumHours = 0;
		int sumSquaredHours = 0;
		
		double sumTime = 0;
		double sumSquaredTime = 0;
		
		double sumClicksIntensity = 0;
		double sumClicksHours = 0;
		double sumIntensityTime = 0;
		double sumHoursTime = 0;
		
		
		int i = 0;
		for(Double clicks : clicksPerSecond)
		{
			sumClicks += clicks;
			sumSquaredClicks += clicks * clicks;
			
			int intensity = intensityValues.get(i);
			sumIntensity += intensity;
			sumSquaredIntensity += intensity * intensity;
			
			int hours = hoursPlayedValues.get(i);
			sumHours += hours;
			sumSquaredHours += hours * hours;

			double time = timeValues.get(i);
			sumTime += time;
			sumSquaredTime += time * time;
			
			sumClicksIntensity += clicks * intensity;
			sumClicksHours += clicks * hours;
			sumIntensityTime += intensity * time;
			sumHoursTime += hours * time;
			i++;
		}

		int n = clicksPerSecond.size();
		
		clicksPerSecondMean = sumClicks / n;
		intensityMean = sumIntensity / n;
		hoursMean = sumHours / n;
		timeMean = sumTime / n;
		
		
		corrCoefIntenseClicks = (n * sumClicksIntensity - sumClicks * sumIntensity)
				/(Math.sqrt((n * sumSquaredClicks - sumClicks * sumClicks)
						*(n * sumSquaredIntensity - sumIntensity * sumIntensity)));
		
		corrCoefHoursClicks = (n * sumClicksHours - sumClicks * sumHours)
				/(Math.sqrt((n * sumSquaredClicks - sumClicks * sumClicks)
						*(n * sumSquaredHours - sumHours * sumHours)));
		
		corrCoefIntenseTime = (n * sumIntensityTime - sumIntensity * sumTime)
				/(Math.sqrt((n * sumSquaredIntensity - sumIntensity * sumIntensity)
						*(n * sumSquaredTime - sumTime * sumTime)));
		
		corrCoefHoursTime = (n * sumHoursTime - sumHours * sumTime)
				/(Math.sqrt((n * sumSquaredHours - sumHours * sumHours)
						*(n * sumSquaredTime - sumTime * sumTime)));
		
		varianceClicks = (sumSquaredClicks - n * clicksPerSecondMean * clicksPerSecondMean)/(n - 1);
		varianceIntensity = (sumSquaredIntensity - n * intensityMean * intensityMean)/(n - 1);
		varianceHours = (sumSquaredHours - n * hoursMean * hoursMean)/(n - 1);
		varianceTime = (sumSquaredTime - n * timeMean * timeMean)/(n - 1);
		
		stDevClicks = Math.sqrt(varianceClicks);
		stDevIntensity = Math.sqrt(varianceIntensity);
		stDevHours = Math.sqrt(varianceHours);
		stDevTime = Math.sqrt(varianceTime);
		
		System.out.println(corrCoefIntenseClicks);
		System.out.println(corrCoefHoursClicks);
		System.out.println(corrCoefIntenseTime);
		System.out.println(corrCoefHoursTime);
	}
	
	public Color[] calcMeanColours()
	{
		int[] means = new int[10];
		Arrays.fill(means, 8355711);
		int[] amounts = new int[10];
		Arrays.fill(amounts, 1);
		int i = 0;
		for (Color colour : colours)
		{
			int intense = intensityValues.get(i);
			
			amounts[intense - 1]++;
			int colourVal = ((int)(colour.r * 255) << 16) + ((int)(colour.g * 255) << 8) + (int)colour.b * 255;
			means[intense - 1] += colourVal;
			i++;
		}
		
		Color[] out = new Color[10];
		
		for (int n = 0; n < 10; n++)
		{
			out[n] = new Color(means[n] / amounts[n]);
		}
		
		return out;
	}
	
	public double stDistClicks(double x)
	{
		double y = Math.pow(Math.E, Math.pow(((x - clicksPerSecondMean) / stDevClicks), 2)/(-2));
		y /= (stDevClicks * Math.sqrt(2 * Math.PI));
		return y;
	}
	
	public double stDistIntensity(double x)
	{
		double y = Math.pow(Math.E, Math.pow(((x - intensityMean) / stDevIntensity), 2)/(-2))
		 / (stDevIntensity * Math.sqrt(2 * Math.PI));
		return y;
	}
	
	public double stDistHours(double x)
	{
		double y = Math.pow(Math.E, Math.pow(((x - hoursMean) / stDevHours), 2)/(-2))
		 / (stDevHours * Math.sqrt(2 * Math.PI));
		return y;
	}
	
	public double stDistTime(double x)
	{
		double y = Math.pow(Math.E, Math.pow(((x - timeMean) / stDevTime), 2)/(-2))
		 / (stDevTime * Math.sqrt(2 * Math.PI));
		return y;
	}
	
	public float corrIntenseClicks(float x)
	{
		float y = (float) (stDevClicks * corrCoefIntenseClicks * 
				((x - intensityMean) / stDevIntensity) + clicksPerSecondMean);
		return y;
	}
	
	public float corrHoursClicks(float x)
	{
		float y = (float) (stDevClicks * corrCoefHoursClicks * 
				((x - hoursMean) / stDevHours) + clicksPerSecondMean);
		return y;
	}
	
	public float corrIntenseTime(float x)
	{
		float y = (float) (stDevTime * corrCoefIntenseTime * 
				((x - intensityMean) / stDevIntensity) + timeMean);
		return y;
	}
	
	public float corrHoursTime(float x)
	{
		float y = (float) (stDevTime * corrCoefHoursTime * 
				((x - hoursMean) / stDevHours) + timeMean);
		return y;
	}
}