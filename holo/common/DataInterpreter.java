package holo.common;

import holo.common.states.*;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class DataInterpreter extends StateBasedGame
{
	public static final int PURPOSE_STATE = 0;
	public static final int INTENSITY_CLICK_STATE = 1;
	public static final int GRADIENT_STATE = 2;
	public static final int HOURS_CLICK_STATE = 3;
	public static final int HOURS_TIME_STATE = 4;
	public static final int INTENSE_TIME_STATE = 5;
	public static final int HOURS_DIST_STATE = 6;
	public static final int INTENSITY_DIST_STATE = 7;
	public static final int TIME_DIST_STATE = 8;
	public static final int CLICK_DIST_STATE = 9;
	public static final int ERROR_STATE = 10;
	
	public DataInterpreter() {
		super("Interpreter");
	}

	
	public static void main(String[] args) throws SlickException 
	{
		AppGameContainer app = new AppGameContainer(new DataInterpreter());
		  
        app.setDisplayMode(app.getScreenWidth(), app.getScreenHeight(), true);
        
        if (app.isUpdatingOnlyWhenVisible())
        	app.setUpdateOnlyWhenVisible(false);
        
        app.setVSync(true);
        app.setShowFPS(false);

        app.start();
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		RawGenerator.createRaw();
		if (!DataHandler.instance().readInfo())
			return;
		
		DataHandler.instance().load();
		DataHandler.instance().do_discreetData();
		this.addState(new Purpose(PURPOSE_STATE, this));
		this.addState(new IntensityClickScatter(INTENSITY_CLICK_STATE, this));
		this.addState(new ColoursGradient(GRADIENT_STATE, this));
		this.addState(new HoursClickScatter(HOURS_CLICK_STATE, this));
		this.addState(new HoursNormalDistributionState(HOURS_DIST_STATE, this));
		this.addState(new TimeNormalDistribution(TIME_DIST_STATE, this));
		this.addState(new IntensityTimeScatter(INTENSE_TIME_STATE, this));
		this.addState(new IntensityNormalDistributionState(INTENSITY_DIST_STATE, this));
		this.addState(new SourcesOfError(ERROR_STATE, this));
	}
}
