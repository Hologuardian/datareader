package holo.common;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class RawGenerator 
{
	static Random rand = new Random();
	static int[] weights = new int[10];
	
	public static void createRaw()
	{
		Arrays.fill(weights, 0);
		weights[0] = rand.nextInt(20);
		for (int i = 1; i < 10; i++)
		{
			weights[i] = weights[i - 1] + rand.nextInt(20);
		}
		
		PrintWriter writer = null;
		try
		{
			File file = new File("Raw.txt");
			if (!file.exists())
				file.createNewFile();
			
			writer = new PrintWriter(file);
			
			for (int i = 0; i < 150; i++)
			{
				int intensity = weightedRandom();
				int time = (int)((rand.nextFloat()) * 1000) * intensity;
				int clicks = rand.nextInt(50) + time / 3000;
				int colour = (rand.nextInt(255) << 16)+(rand.nextInt(255) << 8)+rand.nextInt(255);
				int hours = rand.nextInt(168);
				int likes = rand.nextInt(2);
				writer.println(i + ":" + intensity + ":" + time + ":" + clicks + ":" + colour + ":" + 
						hours + ":" + likes);
			}
			
			writer.close();
		}
		catch (IOException eio)
		{
			eio.printStackTrace();
		}
	}
	
	public static int weightedRandom()
	{
		int val = 0;
		int mod = rand.nextInt(weights[9]);
		
		if(mod < weights[0])
			val = 1;
		else if (mod < weights[1])
			val = 2;
		else if (mod < weights[2])
			val = 3;
		else if (mod < weights[3])
			val = 4;
		else if (mod < weights[4])
			val = 5;
		else if (mod < weights[5])
			val = 6;
		else if (mod < weights[6])
			val = 7;
		else if (mod < weights[7])
			val = 8;
		else if (mod < weights[8])
			val = 9;
		else
			val = 10;
		
		return val;
	}
}
