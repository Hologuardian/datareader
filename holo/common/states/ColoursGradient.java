package holo.common.states;

import holo.common.DataHandler;
import holo.common.DataInterpreter;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class ColoursGradient extends BasicGameState 
{
	Color[] colours;

	StateBasedGame instance;
	int stateID;
	
	public ColoursGradient (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)throws SlickException 
	{
		colours = DataHandler.instance().calcMeanColours();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {

		for(int intensity = 1; intensity < 10; intensity++)
		{
			float x = (intensity * gc.getWidth() / 10) - gc.getWidth() / 20;
			float x1 = ((intensity + 1) * gc.getWidth() / 10) - gc.getWidth() / 20;
			for (float y = (gc.getHeight() / 20); y < gc.getHeight() - gc.getHeight() / 20; y++)
			{
				g.drawGradientLine(x, y, colours[intensity - 1], x1, y, colours[intensity]);
			}
		}

	}
	
	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.HOURS_CLICK_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.INTENSE_TIME_STATE, new FadeOutTransition(), new FadeInTransition());
		}
	}

	@Override
	public int getID() {
		return stateID;
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)throws SlickException {}
}
