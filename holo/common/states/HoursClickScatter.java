package holo.common.states;

import holo.common.DataHandler;
import holo.common.DataInterpreter;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FontUtils;

public class HoursClickScatter extends BasicGameState 
{
	StateBasedGame instance;
	int stateID;
	int gcWidth;
	int gcHeight;

	public HoursClickScatter (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
		gcWidth = game.getContainer().getWidth();
		gcHeight = game.getContainer().getHeight();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		g.setBackground(Color.black);

		g.setColor(Color.white);

		g.drawLine(gc.getWidth() / 20, 0, gc.getWidth() / 20, gc.getHeight() - gc.getHeight() / 20);
		g.drawLine(gc.getHeight() / 20, gc.getHeight() - gc.getHeight() / 20, gc.getWidth(), gc.getHeight() - gc.getHeight() / 20);

		int i = 0;
		double maxClicks = 0;
		int maxHours = 0;
		for(Double n : DataHandler.instance().clicksPerSecond)
		{
			int hours = DataHandler.instance().hoursPlayedValues.get(i);
			if (n > maxClicks)
				maxClicks = n;
			if (hours > maxHours)
				maxHours = hours;
			i++;
		}
		
		float step = 0.25F;
		for (float n = step; n < maxHours + 1; n += step)
		{
			g.setColor(Color.orange);
			g.drawLine(hoursPoint(n - step, maxHours), 
					clickPoint(DataHandler.instance().corrHoursClicks(n - step), maxClicks), 
					hoursPoint(n, maxHours),
					clickPoint(DataHandler.instance().corrHoursClicks(n), maxClicks));
		}
		
		i = 0;
		for(Double clicks : DataHandler.instance().clicksPerSecond)
		{
			int hours = DataHandler.instance().hoursPlayedValues.get(i);
			g.setColor(DataHandler.instance().colours.get(i));
			int y = clickPoint(clicks, maxClicks);
			int x = hoursPoint(hours, maxHours);
			int pointSize = 8;
			g.fillOval(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
			i++;
		}


		//Drawing Intensity Labels

		FontUtils.drawCenter(gc.getDefaultFont(), String.valueOf(maxHours), 
				hoursPoint(maxHours, maxHours), 
				gc.getHeight() - gc.getHeight() / 20, 0);

		String clicksMean = String.valueOf(DataHandler.instance().clicksPerSecondMean);
		clicksMean = clicksMean.substring(0, 5);

		String clicksMax = String.valueOf(maxClicks);
		clicksMax = clicksMax.substring(0, Math.min(clicksMax.length(), 5));
		int lineOffset = gc.getDefaultFont().getLineHeight() / 2;

		FontUtils.drawCenter(gc.getDefaultFont(), clicksMean, gc.getWidth() / 40
				, clickPoint(DataHandler.instance().clicksPerSecondMean, maxClicks) - lineOffset, 0);

		FontUtils.drawCenter(gc.getDefaultFont(), clicksMax, gc.getWidth() / 40
				, clickPoint(1,1) - lineOffset, 0);

		double intercept = DataHandler.instance().corrHoursClicks(0);
		double slope = DataHandler.instance().corrHoursClicks(1) - intercept;

		String corrCoef = String.valueOf(DataHandler.instance().corrCoefHoursClicks);
		corrCoef = corrCoef.substring(0, Math.min(7, corrCoef.length()));
		String sintercept = String.valueOf(intercept);
		sintercept = sintercept.substring(0, Math.min(7, sintercept.length()));
		String sslope = String.valueOf(slope);
		sslope = sslope.substring(0, Math.min(7, sslope.length()));

		FontUtils.drawRight(gc.getDefaultFont(), "Equation: y = " + sslope + "x " + sintercept, gc.getWidth() - gc.getWidth() / 40, 0, 0);
		FontUtils.drawRight(gc.getDefaultFont(), "Correlation Coefficient: " + corrCoef, gc.getWidth() - gc.getWidth() / 40, gc.getDefaultFont().getLineHeight(), 0);

	}

	public int clickPoint(double clicks, double maxClicks)
	{
		return gcHeight - (gcHeight / 20) - (int)((clicks/maxClicks) * (gcHeight - (gcHeight / 10)));
	}

	public int hoursPoint(double hours, double maxHours)
	{
		return (gcWidth / 20) + (int)((hours/maxHours) * (gcWidth - (gcWidth / 10)));
	}

	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.HOURS_DIST_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.INTENSE_TIME_STATE, new FadeOutTransition(), new FadeInTransition());
		}
	}

	@Override
	public int getID() {
		return stateID;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)throws SlickException {}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)throws SlickException {}
}
