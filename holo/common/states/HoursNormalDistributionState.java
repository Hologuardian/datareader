package holo.common.states;

import holo.common.DataHandler;
import holo.common.DataInterpreter;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FontUtils;

public class HoursNormalDistributionState extends BasicGameState 
{

	StateBasedGame instance;
	int stateID;
	
	public HoursNormalDistributionState (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {

		g.setColor(Color.white);
		
		g.drawLine(gc.getWidth() / 20, 0, gc.getWidth() / 20, gc.getHeight());
		g.drawLine(0, gc.getHeight() - gc.getHeight() / 4, gc.getWidth(), gc.getHeight() - gc.getHeight() / 4);
		
		int maxHours = 0;
		for(Integer n : DataHandler.instance().hoursPlayedValues)
		{
			if (n > maxHours)
				maxHours = n;
		}
		
		g.setColor(Color.red);
		for (int i = -3; i < 4; i++)
		{
			float n = (float) (DataHandler.instance().hoursMean + i * DataHandler.instance().stDevHours);
			float y = (float)(gc.getHeight() - gc.getHeight() / 4 - DataHandler.instance().stDistHours((n)) * (gc.getHeight() - gc.getHeight() / 10) * DataHandler.instance().hoursMean);
			float x = (float)(n * gc.getWidth() / maxHours + gc.getWidth() / 20);
			
			g.drawLine(x, gc.getHeight() * 3 / 4, x, y);

			String value = String.valueOf(n);
			value = value.substring(0, Math.min(5, value.length()));

			int stDev = Math.abs(i);
			String val = stDev + " St Dev = " + value;
			if (stDev == 0)
				val = "Mean = " + value;
			
			FontUtils.drawCenter(gc.getDefaultFont(), val, (int) x, gc.getHeight() * 3 / 4, 0);
		}
		
		float step = 0.25F;
		for (float n = step; n < maxHours; n += step)
		{
			g.setColor(Color.green);
			g.drawLine((float)((n - step) * gc.getWidth() / maxHours + gc.getWidth() / 20), 
					(float)(gc.getHeight() - gc.getHeight() / 4 - DataHandler.instance().stDistHours((n - step)) * (gc.getHeight() - gc.getHeight() / 10) * DataHandler.instance().hoursMean), 
					(float)(n * gc.getWidth() / maxHours + (gc.getWidth() / 20)), 
					(float)(gc.getHeight() - gc.getHeight() / 4 - DataHandler.instance().stDistHours((n)) * (gc.getHeight() - gc.getHeight() / 10) * DataHandler.instance().hoursMean));
		}
		
		String stDeviation = String.valueOf(DataHandler.instance().stDevHours);
		stDeviation = stDeviation.substring(0, Math.min(5, stDeviation.length()));

		String mean = String.valueOf(DataHandler.instance().hoursMean);
		mean = mean.substring(0, Math.min(5, mean.length()));
		
		FontUtils.drawRight(gc.getDefaultFont(), "Standard Deviation = " + stDeviation, gc.getWidth() - gc.getWidth() / 40, 0, 0);
		FontUtils.drawRight(gc.getDefaultFont(), "Mean = " + mean, gc.getWidth() - gc.getWidth() / 40, gc.getDefaultFont().getLineHeight(), 0);
		
	}
	
	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.TIME_DIST_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.HOURS_CLICK_STATE, new FadeOutTransition(), new FadeInTransition());
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		// TODO Auto-generated method stub

	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return stateID;
	}

}
