package holo.common.states;

import holo.common.DataHandler;
import holo.common.DataInterpreter;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FontUtils;

public class IntensityTimeScatter extends BasicGameState 
{
	StateBasedGame instance;
	int stateID;
	int gcWidth;
	int gcHeight;

	public IntensityTimeScatter (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
		gcWidth = game.getContainer().getWidth();
		gcHeight = game.getContainer().getHeight();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		g.setBackground(Color.black);

		g.setColor(Color.white);

		g.drawLine(gc.getWidth() / 20, 0, gc.getWidth() / 20, gc.getHeight() - gc.getHeight() / 20);
		g.drawLine(gc.getWidth() / 20, gc.getHeight() - gc.getHeight() / 20, gc.getWidth(), gc.getHeight() - gc.getHeight() / 20);

		int i = 0;
		double maxTime = 0;
		int maxIntense = 0;
		for(Double n : DataHandler.instance().timeValues)
		{
			int intensity = DataHandler.instance().intensityValues.get(i);
			if (n > maxTime)
				maxTime = n;
			if (intensity > maxIntense)
				maxIntense = intensity;
			i++;
		}

		// Intensity axis labels
		for (i = 1; i < 11; i++)
		{
			FontUtils.drawCenter(gc.getDefaultFont(), String.valueOf(i), 
					intensityPoint(i), 
					gc.getHeight() - gc.getHeight() / 40, 0);
		}

		int lineOffset = gc.getDefaultFont().getLineHeight() / 2;
		
		// Hours played axis labels
		for (float j = 0; j <= maxTime + maxTime / 10; j+= maxTime / 10)
		{
			int lineY = timePoint(j, maxTime);
			FontUtils.drawCenter(gc.getDefaultFont(), String.valueOf(j).substring(0, Math.min(5, String.valueOf(j).length()))
					, gc.getWidth() / 40, lineY - lineOffset, 0);
			g.setColor(Color.white);
			g.drawLine(gc.getWidth() / 20, lineY, gc.getWidth(), lineY);
		}
		
		//Dots
		i = 0;
		for(Double clicks : DataHandler.instance().timeValues)
		{
			int intensity = DataHandler.instance().intensityValues.get(i);
			g.setColor(DataHandler.instance().colours.get(i));
			int y = timePoint(clicks, maxTime);
			int x = intensityPoint(intensity);
			int pointSize = 12;
			g.fillOval(x - pointSize / 2, y - pointSize / 2, pointSize, pointSize);
			i++;
		}
		
		float step = 0.25F;
		//Linear regression line
		for (float n = 1 + step; n < maxIntense + 1; n += step)
		{
			g.setColor(Color.orange);
			g.drawLine(intensityPoint(n - step), timePoint(DataHandler.instance().corrIntenseTime(n - step), maxTime), 
					intensityPoint(n), timePoint(DataHandler.instance().corrIntenseTime(n), maxTime));
		}

		//Drawing Intensity Labels

		String timeMean = String.valueOf(DataHandler.instance().timeMean);
		timeMean = timeMean.substring(0, Math.min(timeMean.length(), 5));

		String timeMax = String.valueOf(maxTime);
		timeMax = timeMax.substring(0, Math.min(timeMax.length(), 5));

		double intercept = DataHandler.instance().corrIntenseTime(0);
		double slope = DataHandler.instance().corrIntenseTime(1) - intercept;

		String corrCoef = String.valueOf(DataHandler.instance().corrCoefIntenseTime);
		corrCoef = corrCoef.substring(0, Math.min(7, corrCoef.length()));
		String sintercept = String.valueOf(intercept);
		sintercept = sintercept.substring(0, Math.min(7, sintercept.length()));
		String sslope = String.valueOf(slope);
		sslope = sslope.substring(0, Math.min(7, sslope.length()));

		FontUtils.drawRight(gc.getDefaultFont(), "Equation: y = " + sslope + "x " + sintercept, gc.getWidth() - gc.getWidth() / 40, 0, 0);
		FontUtils.drawRight(gc.getDefaultFont(), "Correlation Coefficient: " + corrCoef, gc.getWidth() - gc.getWidth() / 40, gc.getDefaultFont().getLineHeight(), 0);
		FontUtils.drawLeft(gc.getDefaultFont(), "Game Intensity vs Total Time", gc.getWidth() / 20, 0);
	}

	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.HOURS_CLICK_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.INTENSITY_CLICK_STATE, new FadeOutTransition(), new FadeInTransition());
		}
	}

	public int intensityPoint(double intensity)
	{
		return (int) ( intensity * gcWidth / 10 - (gcWidth / 20));
	}

	public int timePoint(double time, double maxTime)
	{
		return gcHeight - (gcHeight / 20) - (int)((time/maxTime) * (gcHeight - (gcHeight / 10)));
	}

	@Override
	public int getID() {
		return stateID;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)throws SlickException {}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)throws SlickException {}

}
