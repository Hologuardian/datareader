package holo.common.states;

import holo.common.DataHandler;
import holo.common.DataInterpreter;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FontUtils;

public class Purpose extends BasicGameState
{
	StateBasedGame instance;
	int stateID;
	int gcWidth;
	int gcHeight;
	
	int smallStep;
	int largeStep;
	
	final String[] lines = new String[]{"Background:",
		"Outside of school I develop small apps and create add-ons for games that I like.",
		"I wanted to know how long people can go doing a repetitive task before giving up.",
		"This has a lot of importance to me because I want to know how often a game can",
		"have points in it where the player must do a repetitive task several times.",
		"I also wanted to know if players played certian kinds of games, would they be",
		"more likely to quit faster?",
		"&This leads to my hypothesis:",
		"Players who play intense games are less likely to continue a repetitive task.",
		"I ended up making a small program that would ask a few questions about the",
		"intensity of games that they play and how often they play games and if they",
		"enjoy intense games.",
		"&They were then given a choice between 4 colours but had to wait 3 seconds",
		"before they could choose one, this then repeated forever until they quit.",
		"&I also took the average colour that the players gave for my own interests."
	};
	
	public Purpose (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
		gcWidth = game.getContainer().getWidth();
		gcHeight = game.getContainer().getHeight();
		smallStep = game.getContainer().getDefaultFont().getLineHeight();
		largeStep = smallStep * 2;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		float scale = 1.4F;
		g.scale(scale, scale);
		g.translate(-(gc.getWidth() - gc.getWidth() / scale)/2, -(gc.getHeight() - gc.getHeight() / scale)/2);
		int ySpot = gc.getHeight() / 4;
		for (int i = 0; i < lines.length; i++)
		{
			String line = lines[i];
			if (line.startsWith("&"))
			{
				line = line.replace("&", "");
				ySpot += smallStep;
			}
			
			FontUtils.drawCenter(gc.getDefaultFont(), line, gc.getWidth() / 2, ySpot, 0);
			ySpot += smallStep;
		}
		ySpot += largeStep;
		FontUtils.drawCenter(gc.getDefaultFont(), "Total valid data points: " + DataHandler.instance().totalDatums, gc.getWidth() / 2, ySpot, 0);
	}
	
	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.INTENSITY_CLICK_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.ERROR_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_ESCAPE)
		{
			System.exit(0);
		}
	}
	@Override
	public int getID() 
	{
		return stateID;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)throws SlickException {}

	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {}
	
}
