package holo.common.states;

import holo.common.DataInterpreter;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FontUtils;

public class SourcesOfError extends BasicGameState
{
	StateBasedGame instance;
	int stateID;
	int gcWidth;
	int gcHeight;
	
	int smallStep;
	int largeStep;
	
	final String[] lines = new String[]{"Sources of Error:",
		"There are many sources of error that arise from doing a survey",
		"like this.",
		"&-The survey was distributed online",
		"-The instructions were too vague",
		"-Several data points were invalid",
		"-The program would keep updating even if it was not selected",
		"-Not enough data points",
		"-Being a client downloaded application, some people did not trust the survey"
	};
	
	public SourcesOfError (int id, StateBasedGame game)
	{
		instance = game;
		stateID = id;
		gcWidth = game.getContainer().getWidth();
		gcHeight = game.getContainer().getHeight();
		smallStep = game.getContainer().getDefaultFont().getLineHeight();
		largeStep = smallStep * 2;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		float scale = 1.4F;
		g.scale(scale, scale);
		g.translate(-(gc.getWidth() - gc.getWidth() / scale)/2, -(gc.getHeight() - gc.getHeight() / scale)/2);
		int ySpot = gc.getHeight() / 4;
		for (int i = 0; i < lines.length; i++)
		{
			String line = lines[i];
			if (line.startsWith("&"))
			{
				line = line.replace("&", "");
				ySpot += smallStep;
			}
			
			FontUtils.drawCenter(gc.getDefaultFont(), line, gc.getWidth() / 2, ySpot, 0);
			ySpot += smallStep;
		}
	}
	
	@Override
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_RIGHT)
		{
			instance.enterState(DataInterpreter.PURPOSE_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_LEFT)
		{
			instance.enterState(DataInterpreter.INTENSITY_DIST_STATE, new FadeOutTransition(), new FadeInTransition());
		}
		else if (key == Input.KEY_ESCAPE)
		{
			System.exit(0);
		}
	}
	@Override
	public int getID() 
	{
		return stateID;
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)throws SlickException {}

	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException {}
	
}
